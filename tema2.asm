extern puts
extern printf
extern strlen

%define BAD_ARG_EXIT_CODE -1

section .data
filename: db "./input0.dat", 0
inputlen: dd 2263

fmtstr:            db "Key: %d",0xa, 0
usage:             db "Usage: %s <task-no> (task-no can be 1,2,3,4,5,6)", 10, 0
error_no_file:     db "Error: No input file %s", 10, 0
error_cannot_read: db "Error: Cannot read input file %s", 10, 0

xored:            times 1000 db 0

task1_msg:           db "String length = %d", 10, 0

section .text
global main

extern printf

xor_strings:
  ; TODO TASK 1
  push ebp
  mov ebp, esp

  push dword [ebp + 8]                ; Verificam ce lungime are stringul
  call strlen
  add esp, 4

  mov ecx, eax                        ; Salvam lungimea stringului in ecx pentru loop
  mov ebx, [ebp + 8]                  ; Scoatem encoded_string de pe stiva
  mov edx, [ebp + 12]                 ; Scoatem key de pe stiva

  xor_string_loop:                    ; Efectuam XOR byte cu byte
    xor eax, eax
    mov al, byte [ebx + ecx]
    xor al, byte [edx + ecx]
    mov byte [ebx + ecx], al
    loop xor_string_loop

  xor eax, eax                        ; Facem XOR si pentru primul byte (ecx = 0)
  mov al, byte [ebx + ecx]
  xor al, byte [edx + ecx]
  mov byte [ebx + ecx], al

  leave                               ; Rezultatul a fost salvat in ebx
	ret

rolling_xor:
	; TODO TASK 2
  push ebp
  mov ebp, esp

  push dword [ebp + 8]
  call strlen
  add esp, 4

  mov ecx, eax                          ; Salvam lungimea stringului in ecx
  dec ecx
  mov ebx, dword [ebp + 8]              ; Copiem encoded_string in registru

  rolling_xor_loop:          ; Facem XOR intre byte (N) si (N-1)
    xor eax, eax
    mov al, byte [ebx + ecx]
    xor al, byte [ebx + ecx - 1]
    mov byte [ebx + ecx], al
    loop rolling_xor_loop

  leave                               ; Rezultatul a fost salvat in ebx
	ret

hex_to_bin_resolver:     ; Verificam ce valoare corespunde caracterului primit
  push ebp
  mov ebp, esp

  mov al, byte [ebp + 8]

  value_0:
    cmp al, '0'
    jne value_1
    mov al, 0
    jmp hex_to_bin_resolver_end

  value_1:
    cmp al, '1'
    jne value_2
    mov al, 1
    jmp hex_to_bin_resolver_end

  value_2:
    cmp al, '2'
    jne value_3
    mov al, 2
    jmp hex_to_bin_resolver_end

  value_3:
    cmp al, '3'
    jne value_4
    mov al, 3
    jmp hex_to_bin_resolver_end

  value_4:
    cmp al, '4'
    jne value_5
    mov al, 4
    jmp hex_to_bin_resolver_end

  value_5:
    cmp al, '5'
    jne value_6
    mov al, 5
    jmp hex_to_bin_resolver_end

  value_6:
    cmp al, '6'
    jne value_7
    mov al, 6
    jmp hex_to_bin_resolver_end

  value_7:
    cmp al, '7'
    jne value_8
    mov al, 7
    jmp hex_to_bin_resolver_end

  value_8:
    cmp al, '8'
    jne value_9
    mov al, 8
    jmp hex_to_bin_resolver_end

  value_9:
    cmp al, '9'
    jne value_10
    mov al, 9
    jmp hex_to_bin_resolver_end

  value_10:
    cmp al, 'a'
    jne value_11
    mov al, 10
    jmp hex_to_bin_resolver_end

  value_11:
    cmp al, 'b'
    jne value_12
    mov al, 11
    jmp hex_to_bin_resolver_end

  value_12:
    cmp al, 'c'
    jne value_13
    mov al, 12
    jmp hex_to_bin_resolver_end

  value_13:
    cmp al, 'd'
    jne value_14
    mov al, 13
    jmp hex_to_bin_resolver_end

  value_14:
    cmp al, 'e'
    jne value_15
    mov al, 14
    jmp hex_to_bin_resolver_end

  value_15:
    cmp al, 'f'
    jne hex_to_bin_resolver_end
    mov al, 15
    jmp hex_to_bin_resolver_end

  hex_to_bin_resolver_end:
  leave
  ret

hex_to_binary:
  ; Transformam un sir primit ca parametru din hex in bin
  push ebp
  mov ebp, esp

  push dword [ebp + 8]          ; Aflam lungimea stringului
  call strlen
  add esp, 4

  mov ecx, eax                  ; Setam lungimea stringului
  mov ebx, dword [ebp + 8]      ; Mutam adresa stringului in ebx
  mov edx, 0                    ; Contor pentru numarul de caractere procesate
  hex_to_bin_loop:
    xor eax, eax
    mov al, byte [ebx + edx]    ; Extragem un byte din string
    push eax
    call hex_to_bin_resolver    ; Il transformam din ASCII in binar
    add esp, 4                  ; Rezultatul este salvat in AL
    mov ah, al                  ; Mutam rezultatul in AH

    mov al, byte [ebx + edx + 1]  ; Extragem un byte din string
    push eax
    call hex_to_bin_resolver      ; Il transformam din ASCII in binar
    add esp, 4                  ; Rezultatul este salvat in AL

    shl ah, 4                   ; Inmultim AH cu 16
    add al, ah                  ; Adunam in AL pe AH si obtinem noul byte
    push eax                    ; Salvam noul byte pe stiva

    add edx, 2                  ; Sarim din 2 in 2 caractere pentru a traduce bytes
    cmp edx, ecx
    jl hex_to_bin_loop

  shr ecx, 1                        ; Dimensiunea s-a injumatatit
  form_new_bin_string:
    pop eax
    mov byte [ebx + ecx - 1], al    ; Copiem rezultatele de pe stva in string
    loop form_new_bin_string

  leave
  ret

xor_hex_strings:
	; TODO TASK 3
	ret

base32decode:
	; TODO TASK 4
	ret

check_force_substr:
  ; TODO TASK 5
  push ebp
  mov ebp, esp

  mov ebx, [ebp + 8]          ; Luam xored_string de pe stiva
  push ebx
  call strlen                 ; Ii aflam lungimea
  add esp, 4

  mov ecx, eax
  mov edx, 0
  check_substr:           ; Verificam daca stringul "force" se afla in rezultat
    cmp edx, 0
    je check_e
    cmp edx, 1
    je check_c
    cmp edx, 2
    je check_r
    cmp edx, 3
    je check_o
    cmp edx, 4
    je check_f

    jmp check_substr_end          ; Am facut match pe stringul "force"

    check_e:
      inc edx
      cmp byte [ebx + ecx - 1], 'e'
      je ok
      jne reset
    check_c:
      inc edx
      cmp byte [ebx + ecx - 1], 'c'
      je ok
      jne reset
    check_r:
      inc edx
      cmp byte [ebx + ecx - 1], 'r'
      je ok
      jne reset
    check_o:
      inc edx
      cmp byte [ebx + ecx - 1], 'o'
      je ok
      jne reset
    check_f:
      inc edx
      cmp byte [ebx + ecx - 1], 'f'
      je ok
      jne reset

    reset:
      mov edx, 0
    ok:
    loop check_substr
  check_substr_end:

  cmp edx, 5
  je true
  jne false

  false:
    mov edx, 0            ; Returnam 0 daca nu avem "force" in xored
    jmp end_substr
  true:
    mov edx, 1            ; Returnam 1 daca avem "force" in xored
    jmp end_substr

  end_substr:

  leave
	ret

bruteforce_singlebyte_xor:
	; TODO TASK 5
  push ebp
  mov ebp, esp

  mov ebx, [ebp + 8]
  push ebx
  call strlen                 ; Aflam lungimea lui encoded_string
  add esp, 4

  mov ecx, eax
  xor eax, eax
  mov al, 0
  bruteforce_loop:        ; Vom testa valori intre 1 si 255 pentru cheie
    inc al
    cmp al, 0
    je bruteforce_loop_end

    push eax              ; Salvam valorile registrilor
    push ebx
    push ecx

    lea edx, [xored]
    xor_loop:            ; Facem XOR intre encoded_string si cheia EAX
      mov ah, byte [ebx + ecx - 1]
      xor ah, al
      mov byte [edx + ecx - 1], ah
      loop xor_loop

    push edx
    call check_force_substr       ; Verificam daca "force" apare in rezultat
    add esp, 4

    pop ecx             ; Restituim valorile registrilor
    pop ebx
    pop eax

    cmp edx, 1          ; Daca "force" se regaseste in rezultat, am gasit cheia
    jne bruteforce_loop

  bruteforce_loop_end:

  lea ecx, [xored]      ; Returam stringul decodificat

  leave                 ; Cheia va fi in EAX
	ret

decode_vigenere:
	; TODO TASK 6
	ret

main:
  mov ebp, esp; for correct debugging
	push ebp
	mov ebp, esp
	sub esp, 2300

	; test argc
	mov eax, [ebp + 8]
	cmp eax, 2
	jne exit_bad_arg

	; get task no
	mov ebx, [ebp + 12]
	mov eax, [ebx + 4]
	xor ebx, ebx
	mov bl, [eax]
	sub ebx, '0'
	push ebx

	; verify if task no is in range
	cmp ebx, 1
	jb exit_bad_arg
	cmp ebx, 6
	ja exit_bad_arg

	; create the filename
	lea ecx, [filename + 7]
	add bl, '0'
	mov byte [ecx], bl

	; fd = open("./input{i}.dat", O_RDONLY):
	mov eax, 5
	mov ebx, filename
	xor ecx, ecx
	xor edx, edx
	int 0x80
	cmp eax, 0
	jl exit_no_input

	; read(fd, ebp - 2300, inputlen):
	mov ebx, eax
	mov eax, 3
	lea ecx, [ebp-2300]
	mov edx, [inputlen]
	int 0x80
	cmp eax, 0
	jl exit_cannot_read

	; close(fd):
	mov eax, 6
	int 0x80

	; all input{i}.dat contents are now in ecx (address on stack)
	pop eax
	cmp eax, 1
	je task1
	cmp eax, 2
	je task2
	cmp eax, 3
	je task3
	cmp eax, 4
	je task4
	cmp eax, 5
	je task5
	cmp eax, 6
	je task6
	jmp task_done

task1:
	; TASK 1: Simple XOR between two byte streams

	; TODO TASK 1: find the address for the string and the key
  ; Aflam lungimea pentru "encoded_string"
  push ecx                ; Salvam adresa streamului de input pe stiva

  push ecx                ; Verificam ce lungime are stringul
  call strlen
  add esp, 4

  mov edx, eax            ; Salvam lungimea stringului in edx
  inc edx

  pop ecx                 ; Obtinem adresa inputului de pe stiva

  ; TODO TASK 1: call the xor_strings function

  add ecx, edx
  push ecx                ; Punem key pe stiva
  sub ecx, edx
  push ecx                ; Punem encoded_string pe sitva
  call xor_strings        ; Efectuam xor_string -> rezultatul va fi in ebx
  add esp, 8

  push ebx                ; Printam rezultatul
	call puts
	add esp, 4

	jmp task_done

task2:
	; TASK 2: Rolling XOR

	; TODO TASK 2: call the rolling_xor function

  push ecx                ; Salvam adresa lui encoded_string pe stiva
  call rolling_xor        ; Efectuam rolling_xor -> rezultatul va fi in ebx
  add esp, 4

  push ebx                ; Printam rezultatul
	call puts
	add esp, 4

	jmp task_done

task3:
	; TASK 3: XORing strings represented as hex strings

  push ecx              ; Salvam adresa streamului de inceput pe stiva

  push ecx              ; Aflam ce lungime are encoded_string
  call strlen
  add esp, 4

  pop ecx               ; Scoatem de pe stive inceptului streamului
  push ecx              ; Salvam adresa streamului de inceput pe stiva

  inc eax               ; ECX + EAX (lungime string_1) + 1 va fi inceputul "key"
  add ecx, eax          ; Transformam din Hex in Bin "key"
  push ecx
  call hex_to_binary    ; Rezultatul va fi in ebx
  add esp, 4

  pop ecx               ; Scoatem de pe stive inceptului streamului
  push ebx              ; Salvam pe stiva adresa lui key_bin

  push ecx
  call hex_to_binary    ; Transformam din Hex in Bin "encoded_string"
  add esp, 4

  ; TODO TASK 1: find the addresses of both strings
	; TODO TASK 1: call the xor_hex_strings function

  push ebx              ; Salvam pe stiva adresa lui encoded_string_bin
  call xor_strings      ; Apelam functia de la taskul 1 pentru XOR intre stringurile obtinute
  add esp, 8            ; Rezultatul va fi in ebx

  push ebx              ; Printam rezultatul XOR-ului
  call puts
  add esp, 4

	jmp task_done

task4:
	; TASK 4: decoding a base32-encoded string
	; TODO TASK 4: call the base32decode function

	push ecx
	call puts                    ;print resulting string
	pop ecx

	jmp task_done

task5:
	; TASK 5: Find the single-byte key used in a XOR encoding
	; TODO TASK 5: call the bruteforce_singlebyte_xor function

  push ecx
  call bruteforce_singlebyte_xor  ; Cautam cheia
  add esp, 4

  push eax        ; Salvam cheia gasita pe stiva pentru afisare

	push ecx                    ;print resulting string
	call puts
	pop ecx

  pop eax         ; Restituim valoarea cheii in EAX
  push eax                    ;eax = key value
	push fmtstr
	call printf                 ;print key value
	add esp, 8

	jmp task_done

task6:
	; TASK 6: decode Vignere cipher

	; TODO TASK 6: find the addresses for the input string and key
	; TODO TASK 6: call the decode_vigenere function

	push ecx
	call strlen
	pop ecx

	add eax, ecx
	inc eax

	push eax
	push ecx                   ;ecx = address of input string
	call decode_vigenere
	pop ecx
	add esp, 4

	push ecx
	call puts
	add esp, 4

task_done:
	xor eax, eax
	jmp exit

exit_bad_arg:
	mov ebx, [ebp + 12]
	mov ecx , [ebx]
	push ecx
	push usage
	call printf
	add esp, 8
	jmp exit

exit_no_input:
	push filename
	push error_no_file
	call printf
	add esp, 8
	jmp exit

exit_cannot_read:
	push filename
	push error_cannot_read
	call printf
	add esp, 8
	jmp exit

exit:
	mov esp, ebp
	pop ebp
	ret
